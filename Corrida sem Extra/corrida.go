package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const NUM_CORREDORES int = 4 // Número de corredores
const NUM_MAX_SLEEP int = 8  // Tempo (em segundos) máximo que um corredor irá demorar para fazer o seu percurso

var wg sync.WaitGroup                             // WaitGroup para a go routine principal
var corredores = make([]Corredor, NUM_CORREDORES) // Array com corredores
var bastao = make(chan string, 1)                 // Bastão usado na corrida

// Objeto que representa um corredor de uma equipe
type Corredor struct {
	tempo   time.Duration // Tempo que o corredor iraá demorar
	posicao int           // Posiação do corredor entre os corredores da equipe
}

// Funcao em que o Corredor começa a correr, e assim que terminar,
// chama o próximo corredor, se houver um próximo
func correr(co Corredor) {

	<-bastao

	duracao := co.tempo * time.Second
	fmt.Printf("Iniciando || Corredor: %d (%0.f segundos)\n", co.posicao, duracao.Seconds())
	time.Sleep(duracao)

	// Se não for o último chama o próximo corredor
	if co.posicao < NUM_CORREDORES {
		go correr(corredores[co.posicao])
		fmt.Printf("Passando Bastão || Corredor: %d\n", co.posicao)
	} else {
		fmt.Printf("Terminei || Corredor: %d\n", co.posicao)
	}

	wg.Done()

	if co.posicao != NUM_CORREDORES {
		bastao <- "Bastao"
	}
}

func main() {
	fmt.Println("CORRIDA INICIADA !!!\n")

	source := rand.NewSource(time.Now().UnixNano())
	random := rand.New(source)

	// Instaciação dos corredores
	for ii := 0; ii < NUM_CORREDORES; ii++ {
		corredores[ii] = Corredor{time.Duration(random.Intn(NUM_MAX_SLEEP)), (ii + 1)}
	}

	wg.Add(NUM_CORREDORES)

	bastao <- "Bastao"

	// Inicia o primeiro corredor como go routine
	go correr(corredores[0])

	wg.Wait()

	fmt.Println("\nCORRIDA ENCERRADA !!!")
}
