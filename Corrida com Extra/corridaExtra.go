package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"strconv"
	"sync"
	"time"
)

const NUM_EQUIPES int = 3    // Nuúmero de equipes que disputarão a corrida
const NUM_CORREDORES int = 4 // Número de corredores de cada equipe
const NUM_MAX_SLEEP int = 5  // Tempo (em segundos) máximo que um corredor irá demorar para fazer o seu percurso

var wg sync.WaitGroup                 // WaitGroup para a go routine principal
var ch chan string                    // Canal que irá armazenar os nomes das equipes por ordem de chegada
var equipes = make(map[string]Equipe) // Map com Equipes, a chave é o nome da equipe

// Objeto que representa um corredor de uma equipe
type Corredor struct {
	tempo   time.Duration // Tempo que o corredor iraá demorar
	posicao int           // Posiação do corredor entre os corredores da equipe
	equipe  string        // Nome da equipe em que o corredor pertence
}

// Objeto que representa uma equipe na corrida
type Equipe struct {
	corredores []Corredor  // Array com os corredores da equipe
	nome       string      // Nome da equipe
	bastao     chan string // Canal que sera usado como bastao da equipe
}

// Função que instancia uma equipe e à retorna, recebendo como parâmetro o nome da equipe
func criarEquipe(nome string) Equipe {
	return Equipe{make([]Corredor, NUM_CORREDORES), nome, make(chan string, 1)}
}

// Função em que o Corredor começa a correr, e assim que terminar,
// chama o próximo corredor da equipe, se houver um próximo
func correr(co Corredor) {

	<-equipes[co.equipe].bastao

	duracao := co.tempo * time.Second
	fmt.Printf("Iniciando || Corredor: %d (%0.f segundos) || Equipe: %s\n", co.posicao, duracao.Seconds(), co.equipe)
	time.Sleep(duracao)

	// Se for o último da equipe, escreve o nome da equipe no canal,
	// senão, chama o próximo corredor da equipe
	if co.posicao == NUM_CORREDORES {
		ch <- co.equipe
		fmt.Printf("Terminei || Corredor: %d || Equipe: %s\n", co.posicao, co.equipe)
	} else {
		go correr(equipes[co.equipe].corredores[co.posicao])
		fmt.Printf("Passando Bastão || Corredor: %d || Equipe: %s\n", co.posicao, co.equipe)
	}

	wg.Done()

	if co.posicao != NUM_CORREDORES {
		equipes[co.equipe].bastao <- "Bastao"
	}
}

func main() {
	fmt.Println("CORRIDA INICIADA !!!\n")

	ch = make(chan string, NUM_EQUIPES)

	wg.Add(NUM_CORREDORES * NUM_EQUIPES)

	// Cria todas as equipes, com um nome padrão de "Equipe ?" (? = número da equipe)
	for ii := 1; ii <= NUM_EQUIPES; ii++ {
		var nome_equipe bytes.Buffer
		nome_equipe.WriteString("Equipe ")
		nome_equipe.WriteString(strconv.Itoa(ii))

		equipes[nome_equipe.String()] = criarEquipe(nome_equipe.String())
		equipes[nome_equipe.String()].bastao <- "Bastao"
	}

	source := rand.NewSource(time.Now().UnixNano())
	random := rand.New(source)

	// Instancia os corredores de cada equipe
	for ii := 0; ii < NUM_CORREDORES; ii++ {
		for ne := range equipes {
			equipes[ne].corredores[ii] = Corredor{time.Duration(random.Intn(NUM_MAX_SLEEP)), (ii + 1), ne}
		}

	}

	// Inicia o primeiro corredor de cada equipe como go routine
	for ne := range equipes {
		go correr(equipes[ne].corredores[0])
	}

	wg.Wait()

	fmt.Println("\nCORRIDA ENCERRADA !!!\n")
	fmt.Println("COLOCAÇÕES:")

	// Mostra a colocação de todas equipes de acordo com a ordem de escrita no canal
	colocacoes := len(ch)
	for ii := 1; ii <= colocacoes; ii++ {
		fmt.Printf("%dº Lugar: %s\n", ii, <-ch)
	}

}
